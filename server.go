package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

type PlayStoreError string

func (e PlayStoreError) Error() string {
	return string(e)
}

const ErrPlayerNotFound = PlayStoreError("Player not found")
const ErrInvalidPayload = PlayStoreError("Invalid payload")

type PlayStore interface {
	GetPlayerScore(player string) (int, error)
	SetPlayerScore(player string, score int) error
}

type PlayerServer struct {
	store PlayStore
}

func (ps *PlayerServer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		handleGetRequest(ps, rw, req)
		break

	case http.MethodPost:
		handlePostRequest(ps, rw, req)
		break
	}
}

func handleGetRequest(ps *PlayerServer, rw http.ResponseWriter, req *http.Request) {
	player := strings.Trim(req.URL.Path, "/players/")
	score, err := ps.store.GetPlayerScore(player)
	if err != nil {
		rw.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(rw, err.Error())
		return
	}

	fmt.Fprint(rw, score)
}

func handlePostRequest(ps *PlayerServer, rw http.ResponseWriter, req *http.Request) {
	player := strings.Trim(req.URL.Path, "/players/")
	if len(player) == 0 {
		rw.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(rw, ErrPlayerNotFound.Error())
		return
	}

	if req.Body == nil {
		rw.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(rw, ErrInvalidPayload.Error())
		return
	}

	body, err := getRequestBody(req)
	if err != nil || len(body) == 0 {
		rw.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(rw, ErrInvalidPayload.Error())
		return
	}

	score, err := strconv.Atoi(body)
	if err != nil || score > 100 || score < 0 {
		rw.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(rw, ErrInvalidPayload.Error())
		return
	}

	if err := ps.store.SetPlayerScore(player, score); err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusAccepted)
}

func getRequestBody(req *http.Request) (string, error) {
	if req.Body == nil {
		return "", ErrInvalidPayload
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return "", ErrInvalidPayload
	}

	return string(body), nil
}
