package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"sync"
	"testing"
)

type PlayStore interface {
	set(key string, val int)
}

type inMemoryPlayerStore struct {
	data map[string]int
}

func (s *inMemoryPlayerStore) set(key string, val int) {
	s.data[key] = val
}

type Server struct {
	store PlayStore
}

func (s *Server) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	user := strings.Trim(req.URL.Path, "/users/")
	body, _ := ioutil.ReadAll(req.Body)
	score, _ := strconv.Atoi(string(body))
	s.store.set(user, score)

	rw.WriteHeader(200)
}

func TestWrite(t *testing.T) {
	t.Run("testing basic", func(t *testing.T) {
		store := &inMemoryPlayerStore{map[string]int{}}
		server := &Server{store}
		req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/users/%s", "john"), strings.NewReader("40"))
		rsp := httptest.NewRecorder()
		count := 1000
		var wg sync.WaitGroup
		wg.Add(count)

		for i := 0; i < count; i++ {
			go func(w *sync.WaitGroup, val int) {
				server.ServeHTTP(rsp, req)
				w.Done()
			}(&wg, i)
		}

		wg.Wait()
		fmt.Println("Done")
		fmt.Printf("%v", store)
	})
}
