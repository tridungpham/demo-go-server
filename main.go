package main

import "net/http"

func main() {
	server := &PlayerServer{NewInMemoryStore()}
	if err := http.ListenAndServe(":5000", server); err != nil {
		panic(err)
	}
}
