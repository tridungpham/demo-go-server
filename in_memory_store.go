package main

type inMemoryPlayerStore struct {
	data map[string]int
}

func NewInMemoryStore() *inMemoryPlayerStore {
	return &inMemoryPlayerStore{map[string]int{}}
}

func (s *inMemoryPlayerStore) GetPlayerScore(name string) (int, error) {
	if v, ok := s.data[name]; ok {
		return v, nil
	}

	return 0, ErrPlayerNotFound
}

func (s *inMemoryPlayerStore) SetPlayerScore(name string, score int) error {
	s.data[name] = score
	return nil
}
