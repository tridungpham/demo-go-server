package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"testing"
)

type StubPlayerStore struct {
	scores       map[string]int
	scoreSetLogs [][]string
}

func (sps *StubPlayerStore) GetPlayerScore(name string) (int, error) {
	if score, ok := sps.scores[name]; ok {
		return score, nil
	}

	return 0, ErrPlayerNotFound
}

func (sps *StubPlayerStore) SetPlayerScore(name string, score int) error {
	sps.scoreSetLogs = append(sps.scoreSetLogs, []string{name, strconv.Itoa(score)})
	return nil
}

// integration test with inMemory store
func TestInMemoryStore(t *testing.T) {
	t.Run("can store score properly", func(t *testing.T) {
		name := "John"
		store := NewInMemoryStore()
		server := &PlayerServer{store}
		req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/players/%s", name), strings.NewReader("40"))
		rsp := httptest.NewRecorder()
		server.ServeHTTP(rsp, req)
		assertResponseStatus(t, rsp, http.StatusAccepted)
		assertPlayerScore(t, store, name, 40)

		req2, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/players/%s", name), strings.NewReader("90"))
		rsp2 := httptest.NewRecorder()
		server.ServeHTTP(rsp2, req2)
		assertResponseStatus(t, rsp2, http.StatusAccepted)
		assertPlayerScore(t, store, name, 90)
	})

	t.Run("work well with concurrency environment", func(t *testing.T) {
		name := "John"
		store := NewInMemoryStore()
		server := &PlayerServer{store}
		assertPlayerDoesNotHaveScore(t, store, name)
		req, _ := http.NewRequest(
			http.MethodPost,
			fmt.Sprintf("/players/%s", name),
			strings.NewReader("40"),
		)
		rsp := httptest.NewRecorder()
		count := 1000
		var wg sync.WaitGroup
		wg.Add(count)

		for i := 0; i < count; i++ {
			go func(w *sync.WaitGroup) {
				server.ServeHTTP(rsp, req)
				w.Done()
			}(&wg)
		}

		wg.Wait()
		assertPlayerScore(t, store, name, 40)
	})
}

func assertSetScoreLogs(t testing.TB, store StubPlayerStore, expectedLogs [][]string) {
	t.Helper()

	if !reflect.DeepEqual(store.scoreSetLogs, expectedLogs) {
		t.Errorf("expected %v got %v", expectedLogs, store.scoreSetLogs)
	}
}

func assertPlayerDoesNotHaveScore(t testing.TB, store PlayStore, player string) {
	t.Helper()

	if _, err := store.GetPlayerScore(player); err != nil {
		return
	}

	t.Errorf("expect player %s does not have score", player)
}

func assertPlayerScore(t testing.TB, store PlayStore, player string, expectedScore int) {
	t.Helper()

	got, err := store.GetPlayerScore(player)
	if err != nil {
		t.Errorf("expected player has score but not found any")
	}

	if got != expectedScore {
		t.Errorf("expect player has score of %d but got %d", expectedScore, got)
	}
}

func assertResponseBody(t testing.TB, rsp *httptest.ResponseRecorder, want string) {
	t.Helper()
	got := rsp.Body.String()
	if got != want {
		t.Errorf("want %q got %q", want, got)
	}
}

func assertResponseStatus(t testing.TB, rsp *httptest.ResponseRecorder, expected int) {
	t.Helper()
	if rsp.Result().StatusCode != expected {
		t.Errorf("want status %d got %d", expected, rsp.Result().StatusCode)
	}
}

func newGetScoreRequest(name string) *http.Request {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/players/%s", name), nil)
	return req
}
